'X Y zeroing routine for setting/locating the outside corner of a holding fixture, blank, or the table x_y home. 
'Assumes a "cup" (has a bottom), rectangles, circles, etc. work.
'Does NOT do Z zeroing.
'Adapts to whatever the user-units currently are.
'By Scott Worden - Timber Lake Creations - 12/01/2007
'Modified Alan Grover 2/28/2024
'No warranty expressed or implied. ;-)
'Place zero cup "L" on the outside corner of a fixture, blank, or the table.
'Place an appropriate probe (section of drill rod, old tool, or intended cutter) in spindle/router collet.
'  Single straight flute won't work well for this.
'If using a router you should hook a ground clip to the probe also, just as you would for zeroing the Z axis.
'Locate probe over the plate hole before starting and hook the lead to the appropriate input (clip it to your Zzero plate).
'Verify that the input triggers (lights up on the screen) or the routine will attempt to move the z axis at least 3" down before it errors.
'X,Y zero will be the center of the cup when this file finishes

'Determine whether we are in Preview mode or Move/Cut mode
   &modenow = %(22)
   IF &modenow = 1 THEN GOSUB CHANGEMODE

'Load the My_Variables file from Custom Cut 90
  C#,90 'C:\SbParts\Custom

  &unit_scale=1 'default inches
  if %(25) = 1 then &unit_scale=25.4 'metric

MSGBOX("Is the probe positioned above the plate hole and connected? " + &unit_scale,1,Are You Ready!)
 If &msganswer = CANCEL Then END
 
'Initialize Variables
 &xyloc = 0 * &unit_scale 'FIXME: an x and y correction, or offset should go here
 &start_X = %(1)
 &start_Y = %(2)
 &start_Z = %(3)
 &leftstart = 0
 &rightstart = 0
 &bottomstart = 0
 &topstart = 0
 
'FIXME: `vs` should reset automatically when file ends, drop these
'Get current feedrates to reset to when finished
 &start_XYmove_Speed = %(71)
 &start_Zmove_Speed = %(73)
 &start_XYjog_Speed = %(76)
 &start_Zjog_Speed = %(78)
 
'Set speeds for probing
  VS,0.3 * &unit_scale,0.3 * &unit_scale,,,0.6 * &unit_scale,0.6 * &unit_scale
  
 GOSUB TESTINPUT 

'Consistent depth into cup
 GOSUB ZDEPTH
  
'Do locating
  GOSUB LOCATE_X
  GOSUB LOCATE_Y
  'Refine (if start point wasn't very centered)
  GOSUB LOCATE_X
  GOSUB LOCATE_Y
  
'Reset speeds, set X & Y location, raise Z to safe height
  'shouldnt be necessary: 'VS, &start_XYmove_Speed,&start_Zmove_Speed,,,&start_XYjog_Speed,&start_Zjog_Speed
  VA,0,0 'set the cup-center as 0
  JH 'raises to safe-z height
  VA,&xyloc,&xyloc 'Set the X & Y axis location to the plate measurement
  'JH 'if no xy offset, we are at home, otherwise jog there. uncomment if you always have a good safe-z set
  
'Tool is now set to the corner of the fixture, blank, or table. Remember to zero the Z axis to the desired location.
  PAUSE

  END  'This END statement causes the program to end here without dropping through to subroutines.  
   

'*******************************************Subroutines*********************************************** 
 
ZDEPTH:
 GOSUB TESTINPUT

 'Down till bottom
 On Input(1,1) GOTO HITDEPTH
 MZ, &start_Z - 2 * &unit_scale
 GOTO FAILEDEXIT

 HITDEPTH:
  On Input(1,1)
  'Move up enough to clear fillet, consistent height
  JZ, %(3) + 0.1875 * &unit_scale

 RETURN

LOCATE_X:

 GOSUB TESTINPUT

 'Move X axis to get end points of a chord; X center point is at 1/2 this distance.

   'PAUSE 4
   GOSUB TESTINPUT
   On Input(1,1) GOTO HITLEFT
   MX, &start_X - 2 * &unit_scale
   GOTO FAILEDEXIT
  
   HITLEFT:
    On Input(1,1)
    &leftstart = %(1)
    MX, &start_X

    'PAUSE 2
    GOSUB TESTINPUT
    On Input(1,1) GOTO HITRIGHT
    MX, &start_X + 2 * &unit_scale
    GOTO FAILEDEXIT
   
   HITRIGHT:
    On Input(1,1)
    &rightstart = %(1)
    'MX, &start_X
    &center_X = &leftstart + ((&rightstart - &leftstart) / 2)
    JX, &center_X
    &start_X = &center_X

 RETURN
   
LOCATE_Y:
 'Move Y axis to get end points of another chord; Y center point is at 1/2 this distance.

   'PAUSE 2
   GOSUB TESTINPUT
   On Input(1,1) GOTO HITBOTTOM
   MY, &start_Y - 2 * &unit_scale
   GOTO FAILEDEXIT
  
   HITBOTTOM:
    On Input(1,1)
    &bottomstart = %(2)
    MY, &start_Y
    'PAUSE 2
    GOSUB TESTINPUT
    On Input(1,1) GOTO HITTOP
    MY, &start_Y + 2 * &unit_scale
    GOTO FAILEDEXIT
    
   HITTOP:
    On Input(1,1)
    &topstart = %(2)
    'MY, &start_Y
    &center_Y = &bottomstart + ((&topstart - &bottomstart) / 2)
    JY, &center_Y
    &start_Y = &center_Y
     
   RETURN 
    
    
TESTINPUT:
 &test = &my_ZzeroInput + 50
 IF %(&test) = 1 THEN PAUSE 3
 IF %(&test) = 1 THEN GOTO INPUTFAILED
 RETURN
 
INPUTFAILED:
 'Reset the orginal speeds
  'not needed: 'VS, &start_XYmove_Speed,&start_Zmove_Speed,,,&start_XYjog_Speed,&start_Zjog_Speed
  MSGBOX(Input already triggered...Exiting,16,Input Triggered)
  END

FAILEDEXIT:
 MSGBOX(The probe did not make contact in the expected distance...Exiting,16,Failed)
 END

CHANGEMODE:
    'You cannot run this in Preview Mode. Quit and change Move/Cut mode ...
    PAUSE 
    END              'Exit program now
