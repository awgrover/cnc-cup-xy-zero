# CNC Zero-Cup

A zero'ing cup for CNC's that can do touch detection: especially ShopBots. Can be made from plastic/wood.

![finished device](https://gitlab.com/awgrover/cnc-cup-xy-zero/-/raw/main/zero-cup.jpg)

A round cup (around 1" (30mm) diameter, e.g. copper plumbing cap) is embedded in an L shaped tool. The center of the cup can be aligned with the corner of some stock/piece, and a "find center" script will use it to set 0,0 to that corner.

The final device will zero against 1/4" (6.35mm) or thicker stock, but you can change that value to be as thin as you want, making allowance for the material you make the device out of (0.01" out of PVC? probably not).

This device isn't machined out of metal (could be wood, or plastic). There are some nice designs done in metal in a ShopBot [forum thread](https://www.talkshopbot.com/forum/showthread.php?5495-X-Y-and-Z-zeroing-routine).

This can be useful for setting a Zero when your stock/piece is positioned arbitrarily on the machine bed.

This can be useful if you have to remove the stock/piece during machining, and then put it back to continue machining.

This can be useful if your machine does not have functioning "home" limit switches.

The design is parametric, using Spreadsheet values. There shouldn't be any dimensions buried/repeated in sketches, pads, etc. The file adapts to various stock-thicknesses, cup-diameters, final dimensions.

Eventually, there should be instructions on using this device to also do Z-zero.

## How accurate/repeatable is it?

This needs to be tested!

## How To Make It

### Material

Go find a round cup around 1" (30mm) in diameter (a little larger is often nicer), conductive, fairly stiff. The centering script touches the tool against this cup (in the X and Y), so it has to not deform under the fairly small force (like a Z plate).

Copper plumping caps are convenient. The "cup" does not have to be perfectly round, it can be oval, rectangular, etc. The algorithm that finds the center does assume symmetry: mirror symmetry on X, and on Y.

Cut the "cup" down in height, same height as the final device thickness is ideal. Too tall is annoying (and can cause more safe-height/clearance problems). Also, you need to solder a wire pig-tail to the cup, copper is pretty easy: use a torch because the cup acts like a huge heat sink.

Obviously, you'll need some stock to cut the L shapes out of. The design is 2 pieces that need to be (face) glued together. Pick something that you can glue. HDPE is hard to glue, Delrin is also hard to glue. 

Some woods may be dimensionally stable enough for you (consider humidity and temperature variations: we get 15% to 90%+ humidity, 60F to 90F+ here). I think the design minimizes problems with dimensionally instability, except for the hole holding the cup: it might become loose; and if it doesn't distort the straight edges.

Some plastics may be appropriate. ABS or PVC plastics seems like a decent tradeoff. 

Look at the "Page L-With-Cup Layout" for the initial cut dimensions, and the "Page Bottom L Layout" for second piece. Those sould have been combined to give you an easier way of seeing the dimension. TODO! Anyway, 8 1/2" square (about 210mm square) should fit both pieces. Don't forget a little extra for hold-down. And a second piece for redo'ing.

You can change dimensions, making the L shorter/longer, etc.

### Machining Process

The machining steps were carefully designed to be self-calibrating. You don't have to accurately align anything!

I put the instructions in a series of Text Documents in the freecad file. Go through them in order. There are some variations in the process: notably machining the "bottom L" from the same stock, or from different stock.

Do a test cut. Maybe out of corrugated, or MDF or some soft scrap.

You can set various dimensions/settings in the spreadsheet.

Note that I have settings in the first Path Job, "Job-face-mill-to-thickness", and that all subsequent Jobs/Operations have expressions that copy those values. This is unusual in FreeCad*.

This Freecad file has Path jobs/operations in it. You will have to go through each job/operation and make sure it makes sense for your machine. For example, the jobs are setup for a 1/4" bit, with certain speeds, and the post-processor is set for "xopensbp". Remember, change settings in the first Job, which are used via expressions in all other Jobs/Operations.

Please verify all the operations (including safeheights, etc.). I note that the Dressups don't reliably regenerate if you change some value/dimension/etc.: especially dogbones, "extensions", and tags. Changing settings/dimensions should cause any Toponaming breakage).

\* I set the expressions via scripting. The UI for Path objects does not provide a way to remove some of the expressions. This might be considered mean.

### Calibration

Assuming a perfect world, the machining and glueing steps should result in a Zero that aligns the inner corner.

But, we should be able to calibrate it.

To be described. something like: run an initial zero'ing, then test that zero against actual edge, then put in the calibration (correction) into the script. Maybe use a piece of metal stock, and "touch" against that?

### Alternate Machining Steps

Clever/experienced users can figure out their own steps and variations. For example, it is possible to make the device out of 1 piece, rather than glueing (Delrin!): perhaps make an initial "Top L" with cup, then make the Actual device by stacking the "Top L" and "Bottom L" and cut that from the top, flip, use the initial "Top L" to find zero, and cut the "inner ledge" of the Bottom L.

## ShopBot

A script is provided, `cup-xy-zero.sbp`, adapted from a [posting](https://www.talkshopbot.com/forum/showthread.php?5495-X-Y-and-Z-zeroing-routine&s=fa85ca2bcc2d05040a1ca3268d65e017&p=113690#post113690), original script by Scott Worden - Timber Lake Creations - 12/01/2007. It only sets the X,Y to 0,0. Z-zero should be set as normal.

You can just run that script via "load parts". Or add it under the "Cuts" menu (rename to *.sbc, put in SBParts/ dir, with correct name).

Usage: 

1. Set Z-zero as normal (Z-Plate), or do it as the last step below.
pic: aligning w/x
1. Align the stock/piece with the X axis
pic: positioned against a piece
1. Position the L device against the stock/piece
pic: aligator/z-plate
1. Attach the aligator to the Z-plate (or to the place where you would attach the Z-plate)
1. Position the spindle, with a tool, by eyeball, over the cup. Use a "symmetric" bit (even number of flutes, or a rod). 
1. Run the script.
1. Remove the L device.
1. Set Z-zero as normal (Z-Plate), if you haven't already.

Single-flute bits will likely be off-center (since the bit is not symmetric). Probably turn other bits so that the flutes are aligned with X or Y (lest they turn when the bump the edge of the cup)?

